$(window).on("load", function () {
    // Animate loader off screen
    $(".preloader").fadeOut(300);
    $("body").removeClass("noScroll");
});

$(document).ready(function () {
    $(function () {
        if ($('body').is('.portfolio')) {
            const player = new Plyr(document.getElementById('player'), {
                title: 'Example Title',
            });
            $('.play__btn').click(function () {
                $('.portfolio__modal__wrapper').addClass('is-active');
                $('body').addClass('modal__open');
            });
            $('.close__action').click(function () {
                $('.portfolio__modal__wrapper').removeClass('is-active');
                $('body').removeClass('modal__open');
                player.stop();
            });
        }
    });
    //preloader screen counter
    $('.progress > span').each(function () {
        var $this = $(this);
        jQuery({
            Counter: 0
        }).animate({
            Counter: $this.text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function () {
                $this.text(Math.ceil(this.Counter));
            }
        });
    });

    //homepage owl carousel
    var owl = $('.project__carousel');
    owl.on('initialized.owl.carousel', function (event) {
        var items = event.item.count;
        if (items < 2) {
            $('.owl-directions').hide();
        }
    });
    $(".project__carousel").owlCarousel({
        autoPlay: false,
        items: 1,
        margin: 0,
        navigation: true,
        navContainer: '.owl-directions',
        mouseDrag: true,
        touchDrag: true,
        dots: true,
        dotsContainer: ".owl-dots",
        navSpeed: 1200,
        paginationSpeed: 1200,
        callbacks: false
    });
    var project__video__state = false;

    //Speaks for itself. It's a little spicy. The hamburger menu toggle func.
    $('.action__btn').click(function () {
        $('.project__video').toggleClass('loaded');
        $('.project__rendered').toggleClass('loaded');
        project__video__state = !(project__video__state);
        //console.log('action__btn clicked', project__video__state);
    });

    //Modal
    $('.open__grid').click(function () {
        $('.works__modal__wrapper').addClass('is-active');
        $('body').addClass('modal__open');
    });
    $('.close__action').click(function () {
        $('.works__modal__wrapper').removeClass('is-active');
        $('body').removeClass('modal__open');
    });


    // setTimeout(function () {
    //     $('.project__video').addClass('loaded');
    //     project__video__state = true;

    // }, 3000);

    //Burger
    $('.article').click(function () {
        if ($('body').hasClass('nav__open')) {
            closeNav();
        }
    });
    $('.menu__toggle .burger').click(function () {
        $(this).toggleClass('open');
        $('.offcanvas').toggleClass('open');
        $('body').toggleClass('nav__open');
        $('.article').toggleClass('no__scroll');
        playAudio();
        if (project__video__state === true && $(this).hasClass('open')) {
            $('.project__video').removeClass('loaded');
        } else if (project__video__state === true && !$(this).hasClass('open')) {
            $('.project__video').addClass('loaded');
        }
    });
    $('.header__menu__toggle>.burger').click(function () {
        $(this).toggleClass('open');
        $('.offcanvas').toggleClass('open');
        $('body').toggleClass('nav__open');
        //$('body').toggleClass('no__scroll');
        if (project__video__state === true && $(this).hasClass('open')) {
            $('.project__video').removeClass('loaded');
        } else if (project__video__state === true && !$(this).hasClass('open')) {
            $('.project__video').addClass('loaded');
        }
    });

    function openNav() {
        $('.burger').addClass('open');
        $('.offcanvas').addClass('open');
        $('body').addClass('nav__open');
        //$('body').addClass('no__scroll');
    }

    function closeNav() {
        $('.burger').removeClass('open');
        $('.offcanvas').removeClass('open');
        $('body').removeClass('nav__open');
        //$('body').removeClass('no__scroll');
    }
    $(document).keyup(function (e) { // close overlay with esc key
        if (e.keyCode == 27) closeNav();
    });

    $(".menu-item.external__link").click(function () {
        var href = $(this).attr('href');
        // Delay setting the location for one second
        setTimeout(function () {
            window.location = href
        }, 1000);
        if ($("body").hasClass('nav__open')) {
            closeNav();
            $('.animate').each(function () {
                $(this).addClass('animate-out');
            });
        }
        return false;
    });
    $(".menu-item").click(function () {
        var href = $(this).attr('href');
        // Delay setting the location for one second
        setTimeout(function () {
            window.location = href
        }, 1000);
        if ($("body").hasClass('nav__open')) {
            closeNav();
        }
        return false;
    });
    //JS Got Moves
    $("body").mousemove(function (e) {
        //Homepage
        parallaxIt(e, ".project__video", -100);
        parallaxIt(e, ".main__title", -30);
        parallaxIt(e, ".view__case__study", -10);
        parallaxIt(e, ".project__controls", -40);
        //About us
        parallaxIt(e, ".main__title", -30);
        parallaxIt(e, ".description", -20);
        parallaxIt(e, ".what__we__do__list", 10);
        //Contact us
        parallaxIt(e, ".main__title", -30);
        parallaxIt(e, ".description", -20);
    });

    function parallaxIt(e, target, movement) {
        var $this = $("body");
        var relX = e.pageX - $this.offset().left;
        var relY = e.pageY - $this.offset().top;

        TweenMax.to(target, 1, {
            x: (relX - $this.width() / 2) / $this.width() * movement,
            y: (relY - $this.height() / 2) / $this.height() * movement
        });
    }
});

// (function () {
//     var windowHeight = window.innerHeight;
//     var projectItem = document.querySelector('.project__item');
//     var projectItemHeight = projectItem.offsetHeight;

//     console.log(windowHeight, projectItemHeight);

//     projectItem.style.marginTop = ((windowHeight - projectItem) / 2 + " px");
// })();
var menuHover = document.getElementById("menu-audio");
menuHover.volume = 0.2;

function playAudio() {
    menuHover.play();
}
var audioHover = document.getElementById("hover-audio");
audioHover.volume = 0.2;

function playHoverAudio() {
    var promise = document.querySelector('#hover-audio').play();

    if (promise !== undefined) {
        promise.then(_ => {
            // Autoplay started!
            audioHover.play();
        }).catch(error => {
            // Autoplay was prevented.
            // Show a "Play" button so that user can start playback.
        });
    }
}